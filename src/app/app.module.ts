import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { IonicModule } from "@ionic/angular";

import { AppComponent, ModalPageComponent } from "./app.component";
import { ServiceWorkerModule } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { FormsModule } from "@angular/forms";

@NgModule({
  declarations: [AppComponent, ModalPageComponent],
  imports: [
    BrowserModule,
    FormsModule,
    IonicModule.forRoot(),
    ServiceWorkerModule.register("ngsw-worker.js", {
      enabled: environment.production
    })
  ],
  entryComponents: [ModalPageComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
